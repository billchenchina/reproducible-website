---
layout: post
title:  "Reproducible Builds Paris meeting"
date:   2018-09-13 18:37:42
categories: org
draft: false
---

People interested in reproducible builds will meet again! This will be the fourth summit following our [November 2017 summit in Berlin]({{ "/events/berlin2017/" | relative_url }}), the [December 2016 summit in Berlin]({{ "/events/berlin2016/" | relative_url }}) and our [December 2015 summit in Athens]({{ "/events/athens2015/" | relative_url }}).

At this [three-day event in Paris]({{ "/events/paris2018/" | relative_url }}) we will welcome both previous attendees and new projects alike. The participants will discuss, connect and exchange ideas in order to grow the reproducible builds effort. We will bring this space into life:

![]({{ "/images/paris2018/salle-des-fetes.jpg" | relative_url }})

Whilst the exact content of the meeting will be shaped by the participants, the main goals will include:

  * Updating & exchanging the status of reproducible builds in various projects.
  * Improving collaboration both between and inside projects.
  * Expanding the scope and reach of reproducible builds to more projects.
  * Working and hacking together on solutions.
  * Brainstorming designs for tools enabling end-users to get the most benefits from reproducible builds.
  * Discussing how reproducible builds will be usable and meaningful to users and developers alike.

Logs and minutes will be published after the meeting.

Please [reach out]({{ "/events/paris2018" | relative_url }}) if you'd like to participate in hopefully interesting, inspiring and intense technical sessions about reproducible builds and beyond!

